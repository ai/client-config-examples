# Example mail client configurations for Autistici/Inventati

These are some examples for a number of command-line mail client, for receiving, sending, and reading emails.

They should be considered guidelines: every user has its own specific configuration, and his specific needs.
But it you have decide to use such tools, our example are probably sufficient to get you started. More information
can be obtained from Reading The Fucking Manual.
