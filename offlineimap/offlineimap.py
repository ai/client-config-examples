#! /usr/bin/env python
# save as $HOME/.offlineimap.py
from subprocess import check_output

def get_pass(account):
    return check_output("gpg --no-tty --quiet --for-your-eyes-only -d ~/.password-store/" + account + ".gpg", shell=True).rstrip()
